import java.text.DecimalFormat;
	
	public class NumbersToWords {

	  private static final String[] tensNames = {
	    "",
	    " ten",
	    " twenty",
	    " thirty",
	    " forty",
	    " fifty",
	    " sixty",
	    " seventy",
	    " eighty",
	    " ninety"
	  };

	  private static final String[] numNames = {
	    "",
	    " one",
	    " two",
	    " three",
	    " four",
	    " five",
	    " six",
	    " seven",
	    " eight",
	    " nine",
	    " ten",
	    " eleven",
	    " twelve",
	    " thirteen",
	    " fourteen",
	    " fifteen",
	    " sixteen",
	    " seventeen",
	    " eighteen",
	    " nineteen"
	  };

	  private static String below1000(int number) {
	    String soFar;
	    if (number % 100 < 20){
	      soFar = numNames[number % 100];
	      number /= 100;
	    }
	    else {
	      soFar = numNames[number % 10];
	      number /= 10;

	      soFar = tensNames[number % 10] + soFar;
	      number /= 10;
	    }
	    if (number == 0) return soFar;
	    
	    return numNames[number] + " hundred" + soFar;
	  }


	  public static String convert(long number) {
	    // 0 to  999 999 999
	    if (number == 0 ) { return "zero"; }
	    else if(number > 1000000000){return "Number not allowed";}
	   
	    String snumber = Long.toString(number);

	    
	    String mask = "000000000";
	    DecimalFormat df = new DecimalFormat(mask);
	    snumber = df.format(number);

	    int billion = Integer.parseInt(snumber.substring(0,1));
	    
	    int millions  = Integer.parseInt(snumber.substring(1,3));
	    
	    int hundredThousands = Integer.parseInt(snumber.substring(3,6));
	    
	    int thousands = Integer.parseInt(snumber.substring(6,9));

	    String tradBillion;
	    switch (billion) {
	    case 0:
	      tradBillion = "";
	      break;
	    case 1 :
	      tradBillion = below1000(billion)
	      + " billion ";
	      break;
	    default :
	      tradBillion = below1000(billion)
	      + " billion ";
	    }
	    String result =  tradBillion;

	    String tradMillions;
	    switch (millions) {
	    case 0:
	      tradMillions = "";
	      break;
	    case 1 :
	      tradMillions = below1000(millions)
	         + " million ";
	      break;
	    default :
	      tradMillions = below1000(millions)
	         + " million ";
	    }
	    result =  result + tradMillions;

	    String tradHundredThousands;
	    switch (hundredThousands) {
	    case 0:
	      tradHundredThousands = "";
	      break;
	    case 1 :
	      tradHundredThousands = "one thousand ";
	      break;
	    default :
	      tradHundredThousands = below1000(hundredThousands)
	         + " thousand ";
	    }
	    result =  result + tradHundredThousands;

	    String tradThousand;
	    tradThousand = below1000(thousands);
	    result =  result + tradThousand;

	    // remove extra spaces
	    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	  }

	  public static void main(String[] args) {
		  System.out.print("Enter a Number -> ");
		  String input = System.console().readLine();
		  System.out.println(convert(Integer.parseInt(input)));
	  }
	}


