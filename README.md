## Synopsis

Simple Project of a Write it out Program, this time for number

## Code Example

if (number % 100 < 20){  
soFar = numNames[number % 100];  
number /= 100;  
}  
	
## Usage

Console > ../Number_Tell/src > java NumbersToWords  


## Tests

$>java NumbersToWords  
$>Enter a Number -> 56   
$>fifty six  

## Contributors

This is an example taken by: http://www.rgagnon.com/javadetails/java-0426.html

## License

The Unlicense
